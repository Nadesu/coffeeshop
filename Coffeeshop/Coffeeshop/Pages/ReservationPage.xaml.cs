﻿using Coffeeshop.Models;
using Coffeeshop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Coffeeshop.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReservationPage : ContentPage
    {
        public ReservationPage()
        {
            InitializeComponent();
        }


        private async void BtnBookTable_Clicked(object sender, EventArgs e)
        {
            Reservation reservation = new Reservation
            {
                Name = EntName.Text,
                Email = EntEmail.Text,
                Phone = EntPhone.Text,
                TotalPeople = EntTotalPeople.Text,
                Date = DPBookingDate.Date.ToString(),
                Time = TPBookingTime.Time.ToString(),
            };
            ApiServices apiServices = new ApiServices();
            bool response = await apiServices.ReserveTable(reservation);

            if (response != true)
            {
                await DisplayAlert("Oops", "Somthing goes wrong", "Alright");
            }
            else
            {
                await DisplayAlert("Hi", " Your table has been reserved successfully", "Alright");
            }

        }
    }
}