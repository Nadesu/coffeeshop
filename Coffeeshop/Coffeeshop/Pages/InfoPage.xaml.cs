﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Coffeeshop.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoPage : ContentPage
    {
        public InfoPage()
        {
            InitializeComponent();
        }

        private void TapFace_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri ("https://www.instagram.com/"));
        }

        private void TapTwitter_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.instagram.com/"));
        }

        private void TapInstagram_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.instagram.com/"));
        }

        private void TapYoutube_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.instagram.com/"));
        }

        private void TabCall_Tapped(object sender, EventArgs e)
        {
            PhoneDialer.Open("222323232323");

        }
    }
}